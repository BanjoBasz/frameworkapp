<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  @foreach ($frameworks as $framework)
  <form action="/inschrijven" method="post">
        @csrf
        {{$framework->name}}
      <button type="submit" name="framework" value= {{$framework->name}}>Inschrijven</button>
  </form>
  @endforeach
</body>
</html>
