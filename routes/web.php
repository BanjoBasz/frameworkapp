<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/frameworks', 'FrameworksController@index')->name('frameworks_index')->middleware('auth');
Route::get('/frameworks/{id}', 'FrameworksController@show')->name('frameworks_show')->middleware('auth');
Route::get('/inschrijvingen', 'InschrijvingenController@index')->middleware('auth');






Route::post('/inschrijven', 'InschrijvingenController@store')->middleware('auth');
