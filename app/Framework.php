<?php

namespace App;
use App\User;
use App\Inschrijving;
use Illuminate\Database\Eloquent\Model;

class Framework extends Model
{
    protected $table = "frameworks";

    public function cursussen(){
      return $this->hasMany('App\Cursus','framework_id','id');
    }

    public function inschrijvingen(){
      return $this->hasMany('App\Inschrijving','framework_id','id');
    }



}
