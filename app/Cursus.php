<?php

namespace App;
use App\Framework;
use Illuminate\Database\Eloquent\Model;

class Cursus extends Model
{
    protected $table = "cursussen";

    public function framework(){
      return $this->belongsTo('App\Framework','framework_id','id');
    }

}
