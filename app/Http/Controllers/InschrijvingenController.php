<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Framework;
use App\User;
use App\Inschrijving;
class InschrijvingenController extends Controller
{
    public function store(){
      $user = Auth::user();
      $framework = Framework::where('name',Input::get('framework'))->get();
    
       $inschrijving = new Inschrijving();
       $inschrijving->framework_id = $framework[0]->id;
       $inschrijving->user_id = $user->id;

       $inschrijving->save();

        return $inschrijving;

    }

    public function index(){
      return $user =  Auth::user()->inschrijvingen;
    }

}
