<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Framework;
use View;
class FrameworksController extends Controller
{
    public function index(){
      $frameworks = Framework::all();
      return View::make('framework.index',compact('frameworks',$frameworks));
    }

    public function show($id){
      return Framework::find($id);
    }
}
