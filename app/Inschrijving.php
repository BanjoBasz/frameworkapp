<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Inschrijving extends Model
{
    protected $table = "inschrijvingen";
    public $timestamps = false;

    public function user(){
      return $this->belongsTo('App\User','user_id','id');
    }

    public function framework(){
      return $this->belongsTo('App\Framework','framework_id','id');
    }

}
