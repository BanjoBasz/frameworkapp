<?php

use Illuminate\Database\Seeder;

class FrameworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('frameworks')->insert([
          'name' => 'Laravel',
          'language' => 'PHP',
          'end' => 'back-end',
      ]);

      DB::table('frameworks')->insert([
          'name' => 'REACT.JS',
          'language' => 'Javascript',
          'end' => 'front-end',
      ]);

      DB::table('frameworks')->insert([
          'name' => 'Phoenix',
          'language' => 'Javascript',
          'end' => 'back-end',
      ]);
    }
}
