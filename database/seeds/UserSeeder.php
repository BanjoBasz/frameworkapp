<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'password' => bcrypt('laravel'),
          'email' => 'rijsdijk.j@hsleiden.nl',
      ]);

      DB::table('users')->insert([
          'password' => bcrypt('laravel'),
          'email' => 'test@hsleiden.nl',
      ]);

    }
}
