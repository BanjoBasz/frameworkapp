<?php

use Illuminate\Database\Seeder;

class CursusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('cursussen')->insert([
          'framework_id' => 2,
          'beschrijving' => 'Stephen Grinder React cursus',
          'url' => 'https://www.udemy.com/react-redux/learn/v4/'
      ]);

      DB::table('cursussen')->insert([
          'framework_id' => 1,
          'beschrijving' => 'Laravel beginner cursus',
          'url' => 'https://www.udemy.com/php-with-laravel-for-beginners-become-a-master-in-laravel/'
      ]);
    }
}
