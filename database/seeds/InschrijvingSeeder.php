<?php

use Illuminate\Database\Seeder;

class InschrijvingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('inschrijvingen')->insert([
          'framework_id' => 1,
          'user_id' => 2,
      ]);

      DB::table('inschrijvingen')->insert([
          'framework_id' => 1,
          'user_id' => 1,
      ]);

      DB::table('inschrijvingen')->insert([
          'framework_id' => 1,
          'user_id' => 2,
      ]);
    }
}
